﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class tb_pertanyaanViewModel
    {
        public long id { get; set; }
        public long owner_id { get; set; }
        public string pertanyaan { get; set; }
        public System.DateTime created_on { get; set; }
        public bool is_delete { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
    }
}
