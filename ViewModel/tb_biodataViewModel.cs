﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class tb_biodataViewModel
    {
        public long id { get; set; }
        public string Nama_Lengkap { get; set; }
        public string No_Handphone { get; set; }
        public string Email { get; set; }
        public System.DateTime DOB { get; set; }
        public string Alamat { get; set; }
        public bool Jurusan_IT { get; set; }
        public bool deleted { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> modified_by { get; set; }

        public string pertanyaan { get; set; }
    }
}
