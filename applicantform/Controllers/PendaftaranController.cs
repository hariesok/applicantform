﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessModel;
using ViewModel;

namespace applicantform.Controllers
{
    public class PendaftaranController : Controller
    {
        // GET: Pendaftaran
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(tb_biodataViewModel data) {
            try
            {
                BiodataAccessModel.Insert(data);
                return RedirectToAction("List");
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult List() {
            return View(BiodataAccessModel.GetList());
        }

        public ActionResult Edit(long id)
        {
            return View(BiodataAccessModel.GetDetailById(id));
        }

        [HttpPost]
        public ActionResult Edit(tb_biodataViewModel data) {
            try
            {
                BiodataAccessModel.Update(data);
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("List");
        }

        public ActionResult Delete(long Id) {
            BiodataAccessModel.Delete(Id);
            return RedirectToAction("List", "Pendaftaran");
        }

    }
}