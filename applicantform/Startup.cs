﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(applicantform.Startup))]
namespace applicantform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
