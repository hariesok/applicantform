USE [master]
GO
/****** Object:  Database [applicant_db]    Script Date: 7/8/2020 4:11:03 PM ******/
CREATE DATABASE [applicant_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'applicant_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\applicant_db.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'applicant_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\applicant_db_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [applicant_db] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [applicant_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [applicant_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [applicant_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [applicant_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [applicant_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [applicant_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [applicant_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [applicant_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [applicant_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [applicant_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [applicant_db] SET  DISABLE_BROKER 
GO
ALTER DATABASE [applicant_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [applicant_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [applicant_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [applicant_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [applicant_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [applicant_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [applicant_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [applicant_db] SET RECOVERY FULL 
GO
ALTER DATABASE [applicant_db] SET  MULTI_USER 
GO
ALTER DATABASE [applicant_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [applicant_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [applicant_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [applicant_db] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'applicant_db', N'ON'
GO
USE [applicant_db]
GO
/****** Object:  Table [dbo].[tb_biodata]    Script Date: 7/8/2020 4:11:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_biodata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nama_Lengkap] [varchar](50) NOT NULL,
	[No_Handphone] [varchar](16) NOT NULL,
	[Email] [varchar](30) NOT NULL,
	[DOB] [datetime] NOT NULL,
	[Alamat] [varchar](100) NOT NULL,
	[Jurusan_IT] [bit] NOT NULL,
	[deleted] [bit] NOT NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [bigint] NULL,
	[pertanyaan] [varchar](300) NULL,
 CONSTRAINT [PK_tb_biodata] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_pertanyaan]    Script Date: 7/8/2020 4:11:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_pertanyaan](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[owner_id] [bigint] NOT NULL,
	[pertanyaan] [varchar](300) NOT NULL,
	[created_on] [datetime] NOT NULL,
	[is_delete] [bit] NOT NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tb_biodata] ON 

INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (1, N'Haries Chriestian', N'0123456789', N'me@hariesok.co.uk', CAST(0x00008AB500000000 AS DateTime), N'Jalan Petakilan', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (2, N'Haries Chriestian', N'0123456789', N'haries@hariesok.co.uk', CAST(0x00008AB500000000 AS DateTime), N'Jalan Petakilan', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (3, N'Haries Chriestian', N'0123456789', N'admin@hariesok.co.uk', CAST(0x00008AB500000000 AS DateTime), N'Jalan Petakilan', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (5, N'Haries Chriestian', N'0123456789', N'Eli@eli.com', CAST(0x00008AB500000000 AS DateTime), N'Jalan Petakilan', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (6, N'Solid Snake', N'023454567', N'snake@mgs.com', CAST(0x00008AB500000000 AS DateTime), N'Jalan Pegangsaan', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tb_biodata] ([id], [Nama_Lengkap], [No_Handphone], [Email], [DOB], [Alamat], [Jurusan_IT], [deleted], [deleted_on], [deleted_by], [modified_on], [modified_by], [pertanyaan]) VALUES (8, N'Major Zero', N'098765432', N'major@hariesok.co.uk', CAST(0x00008AB500000000 AS DateTime), N'Jalan Pengadilan', 1, 0, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tb_biodata] OFF
SET IDENTITY_INSERT [dbo].[tb_pertanyaan] ON 

INSERT [dbo].[tb_pertanyaan] ([id], [owner_id], [pertanyaan], [created_on], [is_delete], [deleted_on], [deleted_by]) VALUES (1, 1, N'Apakah dapat gaji?', CAST(0x0000ABF100000000 AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tb_pertanyaan] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__tb_bioda__A9D105344114A429]    Script Date: 7/8/2020 4:11:03 PM ******/
ALTER TABLE [dbo].[tb_biodata] ADD UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_biodata] ADD  CONSTRAINT [DF_tb_biodata_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tb_pertanyaan] ADD  DEFAULT ((0)) FOR [is_delete]
GO
USE [master]
GO
ALTER DATABASE [applicant_db] SET  READ_WRITE 
GO
