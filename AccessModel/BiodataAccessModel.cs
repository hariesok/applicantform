﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using DataModel;

namespace AccessModel
{
    public class BiodataAccessModel
    {
        public static string Message = string.Empty;

        public static bool Insert(tb_biodataViewModel data)
        {
            bool result = false;
            try
            {
                using (var db = new applicant_dbEntities())
                {
                    tb_biodata add = new tb_biodata();
                    add.Nama_Lengkap = data.Nama_Lengkap;
                    add.No_Handphone = data.No_Handphone;
                    add.Email = data.Email;
                    add.DOB = data.DOB;
                    add.Alamat = data.Alamat;
                    add.Jurusan_IT = data.Jurusan_IT;
                    add.pertanyaan = data.pertanyaan;
                    db.tb_biodata.Add(add);
                    db.SaveChanges();
                }
                result = true;
                Message = "Data berhasil disimpan.";
            }
            catch (Exception hasError)
            {
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
        public static bool Update(tb_biodataViewModel data)
        {
            bool result = false;
            try
            {
                using (var db = new applicant_dbEntities())
                {
                    tb_biodata s = db.tb_biodata.Where(o => o.id == data.id).FirstOrDefault();
                    if (s != null)
                    {
                        s.Nama_Lengkap = data.Nama_Lengkap;
                        s.No_Handphone = data.No_Handphone;
                        s.Email = data.Email;
                        s.DOB = data.DOB;
                        s.Alamat = data.Alamat;
                        s.Jurusan_IT = data.Jurusan_IT;
                        s.pertanyaan = data.pertanyaan;
                        db.SaveChanges();
                    }
                    else
                    {
                        Message = "Terjadi kesalahan.";
                    }
                }
                result = true;
                Message = "Data berhasil disimpan.";
            }
            catch (Exception hasError)
            {
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<tb_biodataViewModel> GetList()
        {
            List<tb_biodataViewModel> result = new List<tb_biodataViewModel>();
            using (var db = new applicant_dbEntities())
            {
                result = (from data in db.tb_biodata
                          where data.deleted == false
                          select new tb_biodataViewModel
                          {
                              id = data.id,
                              Nama_Lengkap = data.Nama_Lengkap,
                              Email = data.Email,
                              DOB = data.DOB
                          }).ToList();
            }
            return result;
        }

        public static tb_biodataViewModel GetDetailById(long Id)
        {
            tb_biodataViewModel result = new tb_biodataViewModel();

            using (var db = new applicant_dbEntities())
            {
                result = (from a in db.tb_biodata
                          where a.id == Id
                          select new tb_biodataViewModel
                          {
                              id = a.id,
                              Nama_Lengkap = a.Nama_Lengkap,
                              No_Handphone = a.No_Handphone,
                              Email = a.Email,
                              DOB = a.DOB,
                              Alamat = a.Alamat,
                              Jurusan_IT = a.Jurusan_IT,
                              pertanyaan = a.pertanyaan
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Delete(long Id) {
            bool result = false;
            try
            {
                using (var db = new applicant_dbEntities()) {
                    tb_biodata del = db.tb_biodata.Where(o => o.id == Id).FirstOrDefault();
                    if (del != null)
                    {
                        del.deleted = true;
                        del.deleted_by = 1;
                        del.deleted_on = DateTime.Now;

                        db.SaveChanges();
                        Message = "Data berhasil dinonaktifkan.";
                        result = true;
                    }
                    else {
                        Message = "Data tidak ditemukan.";
                    }
                }
            }
            catch (Exception hasError)
            {
                if(hasError != null) {
                    if(hasError.Message.ToLower().Contains("inner exception")) {
                        Message = hasError.InnerException.InnerException.Message;
                    } else {
                        Message = "Maaf terjadi kesalahan.";
                    }
                }
            }
            return result;

        }

    }
}
